<?php
	class Template
	{
		public function __construct()
		{
			$this->ci = &get_instance();
		}
		public function render($path, $data = null){
			$data['meta'] = $this->ci->load->view('_layout/_meta', $data, TRUE);
			$data['css'] = $this->ci->load->view('_layout/_css', $data, TRUE);
			$data['fontsicons'] = $this->ci->load->view('_layout/_fontsicons', $data, TRUE);
			$data['sidebar'] = $this->ci->load->view('_layout/_sidebar', $data, TRUE);
			$data['navbar'] = $this->ci->load->view('_layout/_navbar', $data, TRUE);
			$data['footer'] = $this->ci->load->view('_layout/_footer', $data, TRUE);
			$data['jsplugin'] = $this->ci->load->view('_layout/_jsplugin', $data, TRUE);
			$data['customjs'] = $this->ci->load->view('_layout/_customjs', $data, TRUE);
			$data['page'] = $this->ci->load->view($path, $data, TRUE);
			$this->ci->load->view('_layout/_template.php', $data);
		}
	}
?>