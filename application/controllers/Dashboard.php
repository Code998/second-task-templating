<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends CI_Controller {
	
		public function index()
		{
			$this->template->render('dashboard/dashboard');
		}
	}
	
	/* End of file Home.php */
	/* Location: .//home/banana/Documents/ci-composer-templating/apps/controllers/Home.php */
?>