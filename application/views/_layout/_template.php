<!doctype html>
<html lang="en">
	<head>
	    <?=$meta?>
	    <title>Light Bootstrap Dashboard by Creative Tim</title>
	    <?=$css?>
	    <?=$fontsicons?>
	</head>
	<body>

	<div class="wrapper">
	    <?=$sidebar?>
	    <?=$navbar?>
	    <div class="content">
	        <div class="container-fluid">
						<?=$page?>
	 					<?=$footer?>
	    		</div>
			</div>
	</body>

    <?=$jsplugin?>
    <?=$customjs?>

</html>